# This is a basic pipeline to help you get started with Jtest integration on a Maven project.

stages:
  - test-sa
  - test-ut
  - test-cov

# Builds the project with Maven to run code analysis with Jtest.
StaticAnalysis:
  stage: test-sa
  script:
    # When running on Windows with PowerShell 5.1, be sure to enforce the default file encoding:
      # - $PSDefaultParameterValues['Out-File:Encoding'] = 'default'

    # Configures advanced reporting options and SCM integration.
    - echo "report.format=xml,html,sast-gitlab" > report.properties
    - echo "report.scontrol=min" >> report.properties
    - echo "scontrol.rep.type=git" >> report.properties
    - echo "scontrol.rep.git.url=$CI_PROJECT_URL" >> report.properties
    - echo "scontrol.branch=$CI_COMMIT_BRANCH" >> report.properties
    # When running on Windows, be sure to escape backslashes:
      # - echo "scontrol.rep.git.workspace=$CI_PROJECT_DIR".Replace("\", "\\") >> report.properties
    - echo "scontrol.rep.git.workspace=$CI_PROJECT_DIR" >> report.properties

    # Launches Jtest.
    - echo "Running Jtest..."
    - mvn install jtest:jtest "-Djtest.config=builtin://Recommended Rules" "-Djtest.settings=report.properties" "-Djtest.report=reports"

  artifacts:
    # Uploads analysis results in the GitLab SAST format, so that they are displayed in GitLab.
    reports:
      sast: reports/report.sast
    # Uploads all report files (.xml, .html, .sast) as build artifacts.
    paths:
      - reports/*

# Builds the project with Maven to run unit tests with Jtest.
UnitTests:
  stage: test-ut
  script:
    # Launches Jtest.
    - mvn process-test-classes jtest:agent test jtest:jtest "-Djtest.config=builtin://Unit Tests" "-Djtest.report=reports"

    # Convert Jtest unit tests report to xUnit format.
    # When running on Windows, be sure to replace backslashes:
      # - $CI_PROJECT_DIR = $CI_PROJECT_DIR.Replace("\", "/")
    - path/to/jtest/bin/jre/bin/java -jar 'saxon/saxon-he-12.2.jar' -xsl:"xsl/xunit.xsl" -s:"reports/report.xml" -o:"reports/report-xunit.xml" -t pipelineBuildWorkingDirectory=$CI_PROJECT_DIR
    # Notes: To use Saxon for transformation, a Java executable is required. Jtest bundled with Java which can be used for this purpose.

  artifacts:
    reports:
      junit: reports/report-xunit.xml

# Builds the project with Maven to run code coverage with Jtest.
CodeCoverage:
  stage: test-cov
  script:
    # Launches Jtest.
    - mvn process-test-classes jtest:agent test jtest:jtest "-Djtest.config=builtin://Unit Tests" "-Djtest.report=reports"

    # Convert Jtest coverage report to Cobertura format.
    # When running on Windows, be sure to replace backslashes:
      # - $CI_PROJECT_DIR = $CI_PROJECT_DIR.Replace("\", "/")
    - path/to/jtest/bin/jre/bin/java -jar 'saxon/saxon-he-12.2.jar' -xsl:"xsl/cobertura.xsl" -s:"reports/coverage.xml" -o:"reports/cobertura.xml" -t pipelineBuildWorkingDirectory=$CI_PROJECT_DIR
    # Notes: To use Saxon for transformation, a Java executable is required. Jtest bundled with Java which can be used for this purpose.

    # Uploads code coverage data in the GitLab Cobertura format, so that they are displayed in GitLab.
  artifacts:
    reports:
      coverage_report:
        # Coverage report format.
        coverage_format: cobertura
        # Uploads cobertura report file to enable test coverage visualization in Gitlab merge request.
        path: reports/cobertura.xml
